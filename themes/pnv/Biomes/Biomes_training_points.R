## Preparation of the BIOME 6000 data
## Hengl T, Walsh MG, Sanderman J, Wheeler I, Harrison SP, Prentice IC. 2018. Global mapping of potential natural vegetation: an assessment of machine learning algorithms for estimating land potential. PeerJ 6:e5457 https://doi.org/10.7717/peerj.5457

library(rgdal)
library(plyr)

## available via http://www.bridge.bris.ac.uk/resources/Databases/BIOMES_data
## data set prepared by Sandy Sandy P. Harrison and Iain C. Prentice
biome = read.csv("/data/PNV/Data/Biomes/BIOME_6000_EMBSECBIO_DB_classified_plotfile_v2.csv", na.strings=c("","NA"))
str(biome)
## add training points for Brasil:
radam = readOGR("/data/PNV/Data/Biomes/Radam_Vegetacao_SIRGAS.shp")
radam.leg = read.csv("/data/PNV/Data/Biomes/BR_vegetation_BIOME.csv", na.strings=c("","NA"))
br.biome = spsample(radam, n = 550, type = "random")
br.biome.df = over(br.biome, radam)
br.biome.df$New.global.consolidated.biome.scheme = plyr::join(br.biome.df, radam.leg, by="NM_UVEG")$New.global.consolidated.biome.scheme
summary(as.factor(br.biome.df$New.global.consolidated.biome.scheme))
br.biome.df$Site.Name = paste0("SIM_radam_", 1:nrow(br.biome.df))
br.biome.df$Latitude = br.biome@coords[,2]
br.biome.df$Longitude = br.biome@coords[,1]
br.biome.df$Target.age..ka. = 0
br.biome.df$Remove_points = 0
rm(radam)
## Bind all training dfs
biome_b = plyr::rbind.fill(list(biome[biome$Remove_points==0,], br.biome.df[,c("Site.Name","Latitude","Longitude","Target.age..ka.","Remove_points","New.global.consolidated.biome.scheme")]))
## 11040 rows
## filter out duplicates
## location ID:
biome_b$LOC_ID = paste("ID", round(biome_b$Latitude,4), round(biome_b$Longitude,4), sep="_")
summary(duplicated(biome_b$LOC_ID))
#  Mode   FALSE    TRUE 
# logical    8057    2983
fmode <- function(x) names(table(x))[which.max(table(x))]
## take the dominant class:
dup.biome <- ddply(biome_b[!is.na(biome_b$New.global.consolidated.biome.scheme),], .(LOC_ID), .fun = function(x){fmode(x$New.global.consolidated.biome.scheme)}) ##, .parallel=TRUE)
str(dup.biome)
## if there are only 2 classes then take both
ndup.biome <- ddply(biome_b, .(LOC_ID), summarize, N=length(New.global.consolidated.biome.scheme))
str(ndup.biome)
summary(ndup.biome$N)
summary(ndup.biome$N==2)
## 902 points with 2 observations
## second dominant class:
sel.dup2 = biome_b$LOC_ID %in% ndup.biome$LOC_ID[which(ndup.biome$N==2)]
dup2.biome <- data.frame(LOC_ID=biome_b[sel.dup2,"LOC_ID"], V1=biome_b$New.global.consolidated.biome.scheme[sel.dup2])
str(dup2.biome)
dup.biome[dup.biome$LOC_ID=="ID_-0.25_35.33",]; dup2.biome[dup2.biome$LOC_ID=="ID_-0.25_35.33",]
biome_b[biome_b$LOC_ID=="ID_-0.25_35.33",]

## Bind everything together:
biome_f = plyr::join(do.call(rbind, list(dup.biome[dup.biome$LOC_ID %in% ndup.biome$LOC_ID[which(!ndup.biome$N==2)],], dup2.biome)), biome_b[,c("LOC_ID","Site.Name","Longitude","Latitude","MegaBiomes..Scheme.2.")], match="first")
biome_f = biome_f[!is.na(biome_f$V1),]
biome_f$Biome00k_c = make.names(biome_f$V1)
summary(as.factor(biome_f$Biome00k_c)) 
## cool.mixed.forest = 1560
## temperate.deciduous.broadleaf.forest = 989
## warm.temperate.evergreen.and.mixed.forest = 1012
## steppe = 905
## ...
## 20 generalized levels
## 8797 points / 8057 unique locations

plot(biome_f$Longitude, biome_f$Latitude)
coordinates(biome_f) = ~ Longitude + Latitude
proj4string(biome_f) = "+proj=longlat +datum=WGS84 +ellps=WGS84 +towgs84=0,0,0"
unlink("/data/PNV/Data/Biomes/biome00k_points.gpkg")
#writeOGR(biome_f, "/data/PNV/Data/Biomes/biome00k_points.gpkg", "biome00k_points", "GPKG")
writeOGR(biome_f, "out/gpkg/pnv_biomes.pnts_sites.gpkg", "pnv_biomes.pnts_sites.gpkg", "GPKG")
#plotKML(biome_f["Biome00k_c"], folder.name="Biome.6000.Consolidated.Name", file.name="Biome.6000.Consolidated.Name.kml", kmz=TRUE)
saveRDS(biome_f, "/data/PNV/Data/Biomes/biome_f.rds")
#biome_f = readRDS("/mnt/DATA/PNV/Data/Biomes/biome_f.rds")

biome_f.pnts_sf <- st_as_sf(biome_f[1])
plot_gh(biome_f.pnts_sf, out.pdf="./img/pnv_biomes.pnts_sites.pdf")
system("pdftoppm ./img/pnv_biomes.pnts_sites.pdf ./img/pnv_biomes.pnts_sites -png -f 1 -singlefile")
system("convert -crop 1280x575+36+114 ./img/pnv_biomes.pnts_sites.png ./img/pnv_biomes.pnts_sites.png")

tile.pol = readOGR("../../../tiles/global_tiling_100km_grid.gpkg")
#length(tile.pol)
ov.lc <- extract.tiled(obj=biome_f, tile.pol=tile.pol, path="/data/tt/LandGIS/grid250m", ID="ID", cpus=64)
summary(!is.na(ov.lc$Biome00k_c))
#ov.lc = ov.lc[!is.na(ov.lc$Biome00k_c),]
ov.lc$Biome00k_c = as.factor(make.names(ov.lc$Biome00k_c))
summary(ov.lc$Biome00k_c)
## Valid predictors:
pr.vars = unique(unlist(sapply(c("sm2rain","monthly.temp_worldclim.chelsa","bioclim.var_chelsa","irradiation_solar.atlas", "usgs.ecotapestry", "floodmap.500y", "water.table.depth_deltares", "snow.prob_esacci", "water.vapor_nasa.eo", "wind.speed_terraclimate", "merit.dem_m", "merit.hydro_m", "cloud.fraction_earthenv", "water.occurance_jrc", "wetlands.cw_upmc", "pb2002"), function(i){names(ov.lc)[grep(i, names(ov.lc))]})))
str(pr.vars)
## Final classification matrix:
#rm.lc = ov.lc[complete.cases(ov.lc[,pr.vars]),]

saveRDS(as.data.frame(biome_f), "./out/rds/pnv_biomes.pnts_sites.rds")
#library(farff)
#writeARFF(as.data.frame(biome_f), "./out/arff/pnv_biomes.pnts_sites.arff", overwrite = TRUE)
saveRDS.gz(ov.lc, "../../../out/rds/pnv_biomes.pnts_sites_cm.rds")
